package vote

import  (
	"fmt"
)

type vote struct {
	Title string
	Options []Option
}

type Option struct{
	Name string
	Count int
}

func (v vote) Test() {  
    fmt.Printf(v.Title)
}

func New(Title string, Options []Option ) vote {
	v := vote { Title, Options } 
	return v
}
func (v *vote) AddOption(opt Option){
	v.Options = append(v.Options, opt)
}
func (v *vote) RemoveOptionByIndex(index int){
	v.Options = append(v.Options[:index], v.Options[index+1:]...)
}

func (v *vote) AddVotation(index int){
	v.Options[index].Count += 1
}