package vote_test

import (
	"testing"
	"fmt"
	"../lib/vote"
)
var votation = vote.New ( "FastFood", []vote.Option{{Name: "Hamburguers", Count: 0,},{Name: "Hotdog", Count: 0,} } )


func TestTitle(t *testing.T) {
	// test stuff here...
	if votation.Title != "FastFood" {
		fmt.Println("Title: ", votation.Title)
		t.Fail()
	}
	//
}

func TestQtyOptions(t *testing.T) {
	if len(votation.Options) != 2 {
		t.Fail()
	}
}

func TestAddOpt(t *testing.T) {
	votation.AddOption(vote.Option { Name: "Taco", Count: 0, } )
	if len(votation.Options) != 3 {
		t.Error("Current opts ", votation.Options)
		t.Fail()
	}
}

func TestRmOpt(t *testing.T) {
	votation.RemoveOptionByIndex( 2 )
	if len(votation.Options) != 2 {
		t.Error("Current opts ", votation.Options)
		t.Fail()
	}
}

func TestAddVote(t *testing.T) {
	votation.AddVotation(1)
	votation.AddVotation(1)
	votation.AddVotation(1)
	if votation.Options[1].Count != 3 {
		t.Error("Count for Option #1 ", votation.Options[1].Count)
		t.Fail()
	}
}