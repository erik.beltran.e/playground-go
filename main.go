package main

import (
	"./lib/vote"
	"encoding/json"
	"github.com/rs/cors"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/web"
	"net/http"
	"strconv"
)

var votation = vote.New("FastFood", []vote.Option{{Name: "Hamburguers", Count: 0}, {Name: "Hotdog", Count: 0}})

func main() {

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})
	goji.Use(c.Handler)
	goji.Get("/votation/", getVotation)
	goji.Get("/votation/addOption/:name", addOption)
	goji.Get("/votation/vote/:id", doVotation)
	goji.Serve()
}

func getVotation(c web.C, w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)
	encoder.Encode(votation)
}

func addOption(c web.C, w http.ResponseWriter, r *http.Request) {
	name := c.URLParams["name"]
	votation.AddOption(vote.Option{Name: name, Count: 0})
	encoder := json.NewEncoder(w)
	encoder.Encode(votation.Options)
}

func doVotation(c web.C, w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(c.URLParams["id"])
	encoder := json.NewEncoder(w)
	votation.AddVotation(id)
	encoder.Encode(votation.Options[id])
}
